# 1K Contracts
by [1kcontracts.eper.io](https://1kcontracts.eper.io)

## Who is it for?

The idea of 1K contracts came after the pandemic during the Big Tech Layoffs.

We quickly realized that there are many developers that need jobs.
There were numerous software servicing houses that are short on free cash flow.
Software contractors are traditionally secretive, so a small trade site is an ideal place for some advertising.

Regular job sites have a hard time to keep up in such a situation.
They have existing contracts signed.
They also need to deal with the rise of generative artificial intelligence.
Many companies had to do job cuts.
Budgets keep shrinking due to inflation.

The right approach in such a case is to sign shorter term contracts.
We use the economic term clustering.
People, who visit our site are mainly looking for such software contracts.
This allows you to post more relevant advertising, and we can generate some revenue as well.

## Design

Our site is unique as companies do not need to pay in advance.
They can just place an order for a ticket, get an email and pay later in thirty days.

Tickets are valid for thirty days. They can be downloaded as .coin files with vouchers.
You can just upload this file as a payment, when prompted.

You can post as many ads as you wish.
However, they need to be placed by hand.
Also, they are displayed only for 168 hours.
You need to repost them, when they expired.
This helps us to clean content that are not relevant anymore, when contracts are secured.

The way posting works is that you need to post a Mac Icon sized 1024x1024 png file.
There are many tools that can create such a file, one example is Vectornator,
another on is Gimp.
Feel free to use generative artificial intelligence. It is welcome.

Ads are shown randomly to the users, who visit the site.
We place advertisements of the site ourselves on channels like LinkedIn,
but there is no guarantee of the number of visitors.

We have a leading design mastered by generative tools.
The main site rotates these ads in a random order.
A random rotation ensures that users spend more time here.
We also show both asks and bids.
This allows site users to assess, not just the size of the current conditions.
It does not just help with the big picture.
You can also verify whether it is a buyer's or seller's market.

You need to specify the company name, and billing address to place an order.
We also require an email address.
Please use a company-wide billing address, if it is possible.
We just use the data on the site, we do not resell it.

Nobody can avoid malicious botnets.
We allow users to report unwanted content.
The reason can vary, but these are typically harassing, defamatory, unlawful, or sexual content.
We still show a placeholder for these on the site.
Anybody can uncover the reported flag to verify its validity.
The report recovers, unless the user uncovers it again.

We offer a satisfaction guarantee.
An order can be cancelled anytime.
A refund can be requested as well, if the customer is not satisfied,
even if there are no leads to the post.

Please consult with a local professional for any further regulatory questions.

## Getting started

```
git clone https://gitlab.com/eper.io/1kcontracts.git
```

## License

```
This document is Licensed under Creative Commons CC0.
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
to this document to the public domain worldwide.
This document is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this document.
If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.
```

