package englang

import (
	"bufio"
	"io"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

func Marshal(w io.Writer, r io.Reader) {
	buf := bufio.NewWriter(w)
	lookup := []rune{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}
	b := make([]byte, 1024)
	for {
		n, _ := r.Read(b)
		for i := 0; i < n; i++ {
			_, _ = buf.WriteString(string([]rune{lookup[(b[i]&0xf0)>>4], lookup[b[i]&0x0f], ' '}))
		}
		if n == 0 {
			break
		}
	}
	_ = buf.Flush()
}

func Unmarshal(w io.Writer, r io.Reader) {
	buf := bufio.NewWriter(w)
	b := make([]byte, 1024)
	var bt byte
	var bx byte
	for {
		n, _ := r.Read(b)
		for i := 0; i < n; i++ {
			if b[i] == '\n' || b[i] == ' ' {
				continue
			}
			c := b[i]
			if c >= '0' && c <= '9' {
				bt = (bt << 4) + byte(rune(c)-'0')
				bx++
			}
			if c >= 'a' && c <= 'f' {
				bt = bt<<4 + byte(rune(c)-'0')
				bx++
			}
			if bx == 2 {
				_ = buf.WriteByte(bt)
				bx = 0
				bt = 0
			}
		}
		if n == 0 {
			break
		}
	}
	_ = buf.Flush()
}
