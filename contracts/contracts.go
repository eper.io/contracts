package contracts

import (
	"bytes"
	"fmt"
	"gitlab.com/eper.io/engine/bag"
	"gitlab.com/eper.io/engine/drawing"
	"gitlab.com/eper.io/engine/englang"
	"gitlab.com/eper.io/engine/mesh"
	"gitlab.com/eper.io/engine/metadata"
	"gitlab.com/eper.io/engine/mining"
	"image/color"
	"net/http"
	"strings"
	"time"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

const Home = 0
const CartAndOrder = 1
const RefreshAndSave = 2
const Help = 4
const Report = 3
const Ad = 5

const Instructions = `
%s
1kcontracts.eper.io
This is a site to post 
Upload a 1K x 1K advertisement with the cart.
Click on the green OK button to save.
Click refresh to see all offers.
You can report content with the red button.
Click the question mark for info on the ad.
`

// Setup is the entry screen of the applet.
func Setup() {
	http.HandleFunc("/index.html", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/contracts.html", http.StatusTemporaryRedirect)
	})

	http.HandleFunc("/contracts.html", func(w http.ResponseWriter, r *http.Request) {
		err := drawing.EnsureAPIKey(w, r)
		if err != nil {
			return
		}
		drawing.ServeRemoteForm(w, r, "contracts")
	})

	http.HandleFunc("/contracts.png", func(w http.ResponseWriter, r *http.Request) {
		drawing.ServeRemoteFrame(w, r, declareCorrespondenceForm)
	})
}

func declareCorrespondenceForm(session *drawing.Session) {
	if session.Form.Boxes == nil {
		drawing.DeclareForm(session, "./contracts/res/contracts.png")

		drawing.SetImage(session, Home, "./metadata/logo.png", drawing.Content{Selectable: false, Editable: false})
		drawing.SetImage(session, CartAndOrder, "./contracts/res/cart.png", drawing.Content{Selectable: false, Editable: false})
		drawing.SetImage(session, RefreshAndSave, "./contracts/res/next.png", drawing.Content{Selectable: false, Editable: false})
		drawing.SetImage(session, Report, "./contracts/res/reported.png", drawing.Content{Selectable: false, Editable: false})
		drawing.SetImage(session, Help, "./contracts/res/info.png", drawing.Content{Selectable: false, Editable: false})

		session.SignalRecalculate = func(session *drawing.Session) {
			_, editing := Advertisements[session.ApiKey]
			if editing {
				session.Data = session.ApiKey
				if session.Text[RefreshAndSave].BackgroundFile != "./contracts/res/ok.png" {
					drawing.SetImage(session, RefreshAndSave, "./contracts/res/ok.png", drawing.Content{Selectable: false, Editable: false})
					session.SignalPartialRedrawNeeded(session, RefreshAndSave)
				}
			} else {
				if session.Text[RefreshAndSave].BackgroundFile != "./contracts/res/next.png" {
					drawing.SetImage(session, RefreshAndSave, "./contracts/res/next.png", drawing.Content{Selectable: false, Editable: false})
					session.SignalPartialRedrawNeeded(session, RefreshAndSave)
				}
			}

			if session.Data != "" {
				k := session.Data
				v, ok := Advertisements[k]
				if ok {
					showAd(session, k, v)
					return
				}
			}

			rnd := int(mining.Random(metadata.RandomSalt))
			size := len(Advertisements)
			if size > 0 {
				rnd = rnd % size
				for k, v := range Advertisements {
					if rnd == 0 {
						if showAd(session, k, v) {
							return
						}
					}
					rnd--
				}
			}
		}
		session.SignalClicked = func(session *drawing.Session, i int) {
			if i == Home {
				session.Data = ""
				session.SelectedBox = -1
				session.Redirect = "/"
			}
			if i == RefreshAndSave {
				_, editing := Advertisements[session.ApiKey]
				if editing {
					session.Data = ""
					session.SelectedBox = -1
					session.Redirect = "/"
				} else {
					session.Data = ""
				}
				session.SignalRecalculate(session)
			}
			if i == Help {
				if session.Data != "" {
					ad := session.Data
					status := Advertisements[ad]
					if session.ApiKey == ad {
						// Edit mode
						if session.Text[Ad].Text == "" {
							showInfo(session, englang.Printf(Instructions, "Editing. "+status), ad, Ad)
						} else {
							showAd(session, ad, status)
						}
					} else {
						// Info mode
						if session.Text[Ad].Text == "" {
							showInfo(session, englang.Printf(Instructions, "Viewing. "+status), ad, Ad)
						} else {
							showAd(session, ad, status)
						}
					}
				}
			}
			if i == CartAndOrder {
				session.Upload = ".png"
			}
			if i == Report {
				if session.Data != "" {
					ad := session.Data
					status := Advertisements[ad]
					if status == "Ad is valid." {
						Advertisements[ad] = "Ad is reported for fraud, unlawful, defamatory, or sexual content."
						session.SignalRecalculate(session)
					}
					if status == "Ad is reported for fraud, unlawful, defamatory, or sexual content." {
						Advertisements[ad] = "Ad is rechecked."
						session.SignalRecalculate(session)
						go func(revert string) {
							time.Sleep(10 * time.Second)
							if Advertisements[revert] == "Ad is rechecked." {
								Advertisements[revert] = "Ad is reported for fraud, unlawful, defamatory, or sexual content."
								session.SignalRecalculate(session)
							}
						}(ad)
					}
					if status == "Ad is rechecked." {
						Advertisements[ad] = "Ad is valid."
						session.SignalRecalculate(session)
					}
				}
			}
			if i == Ad {
				ad := session.Data
				status := Advertisements[ad]
				var link string
				if nil == englang.Scanf(status, "Forward leads to %s external link.", &link) {
					session.Redirect = link
				}
			}
		}
		session.SignalTextChange = func(session *drawing.Session, i int, from string, to string) {
			session.SignalPartialRedrawNeeded(session, i)
		}
		session.SignalUploaded = func(session *drawing.Session, upload drawing.Upload) {
			if upload.Type == ".png" {
				//Upload image to a new sack
				x := session.ApiKey
				_, editing := Advertisements[x]
				if !editing {
					bag.MakeBagInternal(x)
				}
				buffer := bytes.NewBuffer(upload.Body)
				_, _ = (&http.Client{}).Do(drawing.NoErrorRequest(http.NewRequest("PUT", mesh.WhoAmI+"/tmp?apikey="+x, buffer)))
				lock.Lock()
				Advertisements[x] = "Ad is valid."
				mesh.SetExpiry(x, AdPeriod)
				lock.Unlock()
				session.SignalRecalculate(session)
				return
			}
			fmt.Println("Cannot handle", upload.Type)
		}
		session.SignalRecalculate(session)
		session.SignalFullRedrawNeeded(session)
	}
}

func showAd(session *drawing.Session, k string, v string) bool {
	if !mesh.CheckExpiry(k) {
		return false
	}
	if v == "Ad is rechecked." {
		drawing.SetImage(session, Report, "./contracts/res/ok.png", drawing.Content{Selectable: false, Editable: false})
		session.SignalPartialRedrawNeeded(session, Report)
	} else {
		drawing.SetImage(session, Report, "./contracts/res/reported.png", drawing.Content{Selectable: false, Editable: false})
		session.SignalPartialRedrawNeeded(session, Report)
	}
	if v == "Ad is valid." || v == "Ad is rechecked." {
		ad := bag.GetBagPathInternal(k)
		session.Data = k
		if strings.HasPrefix(mesh.WhoAmI, "http://127") {
			fmt.Println(ad)
		}
		drawing.SetImage(session, Ad, ad, drawing.Content{Selectable: false, Editable: false})
		session.SignalPartialRedrawNeeded(session, Ad)
		return true
	}
	if v == "Ad is reported for fraud, unlawful, defamatory, or sexual content." {
		drawing.SetImage(session, Ad, "./contracts/res/reported.png", drawing.Content{Selectable: false, Editable: false})
		session.SignalPartialRedrawNeeded(session, Ad)
	}
	return false
}

func showInfo(session *drawing.Session, v string, k string, Ad int) {
	ad := bag.GetBagPathInternal(k)
	session.Data = k
	if strings.HasPrefix(mesh.WhoAmI, "http://127") {
		fmt.Println(ad)
	}
	lines := 24
	//len(strings.Split(v, "\n"))
	//if lines < 24 {
	//	lines = 24
	//}

	drawing.PutText(session, Ad, drawing.Content{Text: v, Lines: lines, Editable: false, Selectable: false, FontColor: color.RGBA64{R: 0x0000, G: 0x2222, B: 0x0000, A: 0xFFFF}, BackgroundColor: drawing.Black, Alignment: 1})
	session.SignalPartialRedrawNeeded(session, Ad)
}
