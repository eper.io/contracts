package contracts

import (
	"bufio"
	"gitlab.com/eper.io/engine/drawing"
	"gitlab.com/eper.io/engine/englang"
	"io"
	"sync"
	"time"
)

var lock sync.Mutex

var AdPeriod = 168 * time.Hour
var Advertisements = map[string]string{}
var PremiumLines int64 = 300

func LogSnapshot(m string, w io.Writer, r io.Reader) {
	if m == "GET" {
		ww := bufio.NewWriter(w)
		for k, v := range Advertisements {
			_, _ = ww.WriteString(englang.Printf("Advertisement record %s of %s bytes follows.\n", k, englang.DecimalString(int64(len([]byte(v))))))
			_, _ = ww.WriteString(v)
		}
	}
	if m == "PUT" {
		blob := drawing.NoErrorString(io.ReadAll(r))
		for {
			var prefix, key, length, suffix string
			if nil == englang.Scanf(blob, "Advertisement record %s of %s bytes follows.\n", &prefix, &key, &length, &suffix) {
				ff := []byte(suffix)
				n := englang.Decimal(length)
				Advertisements[key] = string(ff[0:n])
				blob = string(ff[n:])
			}
		}
	}
}
